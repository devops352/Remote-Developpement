#!/bin/bash

function _check_file()
{
    # check a if a file changed
    local SUM=$(md5sum "$@" | cut -d ' ' -f1)
    if [ ${TAMPON[${FILE}]} != ${SUM} ]
    then
        TAMPON[${FILE}]=${SUM}
        return 0
    else
      return 1
    fi

}

if [[ -z $1 && -z $2 ]]
then 
	echo "you have to set an ip and the module you want to dev"
	exit 1
fi 
#Developpement location
if [[ -z ${3} ]]
then 
    DEV_PATH="synbase/www3/metadata/" 
else
    DEV_PATH=${3}
fi

if [[ -z ${4} ]]
then 
    DEST_PATH=""
else
    DEST_PATH=${4}
fi 
DEST_ROOT=/home/${2}/${DEST_PATH}
# Initalize the tampon before starting the check of file
echo "DEV on ${2} location ${DEV_PATH}"
echo "initialization"
declare -A TAMPON
for FILE in $(find ${DEV_PATH} -type f -name "*.py")
do
  TAMPON[${FILE}]=$(md5sum ${FILE} | cut -d ' ' -f1)
done

# Loop on files to check modifications
while true
do
    for FILE in $(find ${DEV_PATH} -type f -name "*.py")
    do
       if _check_file ${FILE}
       then 
         echo "copy ${FILE} on ${DEST_ROOT}/${FILE#${DEV_PATH}}"
         sshpass -p ${2} scp ${FILE} ${2}@${1}:${DEST_ROOT}/${FILE#${DEV_PATH}}
       fi
    done
    sleep 2
done

